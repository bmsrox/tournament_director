
export const loadTournamentData = (context, payload) => {
  context.commit('LOAD_TOURNAMENT_DATA', payload)
}

export const removeTournamentData = (context) => {
  context.commit('REMOVE_TOURNAMENT_DATA')
}
