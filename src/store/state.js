import getters from './getters'

export default {
  scope: {
    paused: false,
    break: false,
    finished: false,
    current_blind: {
      round:  1,
      small_blind:  null,
      big_blind:  null,
      ante: null,
      duration: 0,
    },
    next_blind: {},
    next_break: 0,
    elapsed: 0,
    background: 'blue',
    tournament: {
      id: getters.tournamentId(), 
      name: null,
      description: null,
    },
    structure: [],
    statistic: {
      rebuys: null,
      addons: null,
      stacks: 0,
      entries: 0,
      players_in: 0,
      players_out: 0
    },
    prize: []
  }
}
