export const STORAGE_KEY = 'tournament_';

export default {
  'LOAD_TOURNAMENT_DATA' (state, payload) {
      save(payload.tournament.id, payload)
      state.scope = tournamentScope(payload)
  },
  'REMOVE_TOURNAMENT_DATA' (state) {
      remove(state.scope.tournament.id)
  }
}

const tournamentScope = (data, round, key) => {

    if (null === data)
      return {};

    let current_round = (typeof round !== "undefined") ? parseInt(round) - 1 : data.tournament.current_round - 1;
    let next_round = current_round + 1;
    let current_blind = data.blinds[current_round];

    let avg_stack = (data.statistic.players_in > 0) ? Math.round(data.statistic.stacks/data.statistic.players_in) : 0;
    data.statistic.avg_stack = avg_stack;

    let scope = {
      paused: false,
      break: false,
      finished: false,
      current_blind: current_blind,
      next_blind: data.blinds[next_round],
      next_break: nextBreak(data.blinds, current_blind.round),
      elapsed: elapsedTime(data.tournament.start_date),
      background: ((current_round % 2) === 0) ? 'blue' : 'green',
      tournament: {
        id: data.tournament.id,
        name: data.tournament.name,
        description: data.tournament.description,
        prize_pool: data.tournament.prize_pool,
      },
      structure: data.structure,
      statistic: data.statistic,
      prize: formatPrize(data)
    }

    return (typeof scope[key] !== "undefined") ? scope[key] : scope;
}

const formatPrize = (data) => {
  if (data.prize !== null) {
    data.prize.map(function(item){
          item.amount = data.tournament.prize_pool * (item.percent/100); //add amount into array
          return item;
      });
  } else {
    data.prize = []
  }

  return data.prize;
}

const elapsedTime = (start_date) => {
    let start_tournament = new Date(start_date).getTime();
    let now = new Date().getTime();

    return (start_tournament) ? parseInt((now-start_tournament)/1000) : 0
}

const nextBreak = (blinds, round) => {

    let next_break = [];

    blinds.map(function (item) {
        item.duration = item.duration * 60 ; //convert to seconds
        item.time_break = parseInt(item.time_break) * 60;
        return item;
    }).filter(function (item) {
        return item.round >= round; //o round é dinamico
    }).reduce(function (acc, item) {
        acc = acc + item.duration;
        if (item.time_break){
            next_break.push(acc);
            acc = 0;
        }
        return acc;
    }, 0);

    return (next_break.length) ? next_break.shift() : 0;
}

const save = (tournament_id, data) => {
  localStorage.setItem(STORAGE_KEY + tournament_id, JSON.stringify(data))
}

const find = (tournament_id) => {
  return JSON.parse(localStorage.getItem(STORAGE_KEY + tournament_id))
}

const remove = (tournament_id) => {
  localStorage.removeItem(STORAGE_KEY + tournament_id)
}
