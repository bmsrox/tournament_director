import Vue from 'vue'
import App from './App.vue'
import store from './store/store'

Vue.filter('stack', stack => {
  if (stack < 1000)
      return stack;

  var thousands = Math.round(stack / 100) / 10;
  // Now convert it to a string and add the k
  return thousands.toString() + 'K';
});

Vue.filter('money', value => {
    return (value) ? "R$ " + value : 0;
});

new Vue({
  store,
  el: '#app',
  render: h => h(App)
})
